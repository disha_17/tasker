import React, { useState } from "react";

import { Header } from "./Header";
import { Container } from "@mui/material";
import { TasksDashboard } from "./TasksDashboard";
import { TaskForm } from "./TaskForm";
import { v4 as uuidv4 } from "uuid";
import { SearchForm } from "./SearchForm";

const App = () => {
  const [task, setTask] = useState("");
  const [tasks, setTasks] = useState([]);
  const [searchTask, setSearchTask] = useState("");
  const [priority, setPriority] = useState("low");
  const [priorityFilter, setPriorityFilter] = useState(2);

  function clearTask() {
    console.log("CLear");
    setTask("");
  }

  function submitTask(evt) {
    evt.preventDefault();
    if (task === "") {
      alert("Blank input not allowed");
    } else {
      console.log("Submit button clicked");
      setTasks(
        tasks.concat({
          id: uuidv4(),
          value: task.toLowerCase(),
          isCompleted: false,
          priority: priority,
          priorityValue: priority === "low" ? 0 : priority === "high" ? 2 : 1,
        })
      );
      setTask("");
    }
  }

  function handleToggleCompletion(taskId) {
    setTasks(
      tasks.map((task) =>
        task.id === taskId ? { ...task, isCompleted: !task.isCompleted } : task
      )
    );
  }

  function handleDelete(taskId) {
    setTasks(tasks.filter((task) => task.id !== taskId));
  }

  return (
    <div>
      <Header />
      <Container maxWidth="lg" sx={{ marginTop: 4 }}>
        <SearchForm
          searchTask={searchTask}
          onSearch={setSearchTask}
          priorityFilter={priorityFilter}
          onPriorityFilter={setPriorityFilter}
        />
        <TaskForm
          task={task}
          onchange={setTask}
          onclear={clearTask}
          onsubmit={submitTask}
          priority={priority}
          onPriorityChange={setPriority}
        />

        <TasksDashboard
          tasks={tasks}
          onToggle={handleToggleCompletion}
          onDelete={handleDelete}
          searchTask={searchTask}
          priority={priority}
          priorityFilter={priorityFilter}
        />
      </Container>
    </div>
  );
};

export default App;
