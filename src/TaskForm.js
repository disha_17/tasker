import {
  Button,
  Card,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import React from "react";
import DataSaverOnOutlinedIcon from "@mui/icons-material/DataSaverOnOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";

export const TaskForm = ({
  task,
  onchange,
  onclear,
  onsubmit,
  priority,
  onPriorityChange,
}) => {
  function handleChange(evt) {
    /*console.log(evt.target.value); */
    onchange(evt.target.value);
  }

  function handlePriorityChange(evt) {
    console.log(evt.target.value);
    onPriorityChange(evt.target.value);
  }

  return (
    <Grid container justifyContent="center" sx={{ marginTop: 3 }}>
      <Grid item xs={12}>
        <Card sx={{ padding: 4 }} raised={true}>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={8}>
              <TextField
                id="task-input"
                label="Insert your task"
                fullWidth
                value={task}
                onChange={handleChange}
              />
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={onsubmit}
              >
                <DataSaverOnOutlinedIcon /> Submit
              </Button>
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="contained"
                color="secondary"
                fullWidth
                onClick={onclear}
              >
                <DeleteOutlinedIcon /> Clear
              </Button>
            </Grid>
          </Grid>
          <Grid container padding={2}>
            <Grid item xs={6}>
              <InputLabel id="demo-simple-select-label">Priority</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={priority}
                onChange={handlePriorityChange}
              >
                <MenuItem value="low">Low</MenuItem>
                <MenuItem value="medium">Medium</MenuItem>
                <MenuItem value="high">High</MenuItem>
              </Select>
            </Grid>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
};
