import {
  Button,
  Checkbox,
  Chip,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@mui/material";
import React from "react";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";

export const Task = ({ task, onToggle, onDelete }) => {
  const textDecorationStyle = task.isCompleted
    ? { textDecoration: "line-through" }
    : {};
  let chipElement;
  if (task.isCompleted) {
    chipElement = <Chip label="Completed" variant="outlined" color="success" />;
  } else {
    chipElement = <Chip label="Pending" variant="outlined" color="warning" />;
  }
  let priorityChipElement = (
    <Chip
      label={task.priority}
      variant="outlined"
      color="info"
      sx={{ margin: 1 }}
    />
  );
  return (
    <ListItem
      key={1}
      sx={{
        paddingBottom: 2,
        paddingTop: 2,
        borderBottom: "solid 1px #AAA",
      }}
      disablePadding
    >
      <Checkbox checked={task.isCompleted} onChange={() => onToggle(task.id)} />
      <ListItemText primary={task.value} sx={textDecorationStyle} />

      <ListItemSecondaryAction>
        {priorityChipElement}
        {chipElement}
        <Button
          variant="contained"
          color="error"
          sx={{ marginLeft: 5 }}
          onClick={() => onDelete(task.id)}
        >
          <DeleteForeverOutlinedIcon /> Delete
        </Button>
      </ListItemSecondaryAction>
    </ListItem>
  );
};
