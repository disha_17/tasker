import { Button } from "@mui/base";
import {
  Card,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import DataSaverOnOutlinedIcon from "@mui/icons-material/DataSaverOnOutlined";
import React from "react";

export const SearchForm = ({
  searchTask,
  onSearch,
  priorityFilter,
  onPriorityFilter,
}) => {
  function handleSearch(evt) {
    onSearch(evt.target.value.toLowerCase());
  }

  function handlePriorityFilterChange(evt) {
    console.log(evt.target.value);
    onPriorityFilter(evt.target.value);
  }
  return (
    <Card sx={{ padding: 2 }} raised={true}>
      <Grid container spacing={2} alignItems="center">
        <Grid item xs={8}>
          <TextField
            id="search-input"
            value={searchTask}
            label="Search your task"
            onChange={handleSearch}
            fullWidth
          />
        </Grid>
        <Grid item xs={4}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Priority</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={priorityFilter}
              onChange={handlePriorityFilterChange}
            >
              <MenuItem value={0}>Low to High</MenuItem>
              <MenuItem value={1}>High To Low</MenuItem>
              <MenuItem value={2}>Default</MenuItem>
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    </Card>
  );
};
