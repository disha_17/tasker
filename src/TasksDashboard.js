import React from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Card,
  TextField,
  Button,
  Container,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox,
  Chip,
} from "@mui/material";
import { TaskList } from "./TaskList";

export const TasksDashboard = ({
  tasks,
  onToggle,
  onDelete,
  searchTask,
  priorityFilter,
}) => {
  return (
    <Grid container justifyContent="center" sx={{ marginTop: 4 }}>
      <Grid item xs={12}>
        <Card sx={{ padding: 1 }} raised={true}>
          <TaskList
            tasks={tasks}
            onToggle={onToggle}
            onDelete={onDelete}
            searchTask={searchTask}
            priorityFilter={priorityFilter}
          />
        </Card>
      </Grid>
    </Grid>
  );
};
