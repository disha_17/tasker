import {
  Button,
  Checkbox,
  Chip,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
} from "@mui/material";
import React from "react";
import { Task } from "./Task";

export const TaskList = ({
  tasks,
  onToggle,
  onDelete,
  searchTask,
  priorityFilter,
}) => {
  // let PriorityFilteredTasks = tasks.sort(
  //   (a, b) => b.priorityValue - a.priorityValue
  // );
  let arrSort = [...tasks];
  let PriorityFilteredTasks =
    priorityFilter === 0
      ? arrSort.sort((a, b) => a.priorityValue - b.priorityValue)
      : priorityFilter === 1
      ? arrSort.sort((a, b) => b.priorityValue - a.priorityValue)
      : tasks;

  let tasksFiltered = PriorityFilteredTasks.filter((task) =>
    task.value.includes(searchTask)
  );
  tasksFiltered = tasksFiltered.toString() === "" ? tasks : tasksFiltered;
  return (
    <List sx={{ padding: 1 }}>
      {/* {tasks.map((task) => (
        <Task
          task={task}
          key={task.id}
          onToggle={onToggle}
          onDelete={onDelete}
        />
      ))} */}

      {tasksFiltered.map((task) => (
        <Task
          task={task}
          key={task.id}
          onToggle={onToggle}
          onDelete={onDelete}
        />
      ))}
    </List>
  );
};
